package lesson4;

/**
 * Написать программу, которая проверяет, является ли строка палиндромом
 */
public class Task1 {
    public static void main(String[] args) {
        String s= "java Palindrome madam racecar apple kayak song noon";
        System.out.println("Word\tReverse\tChecking");
        for (String word: s.split("\s")){
            System.out.print(word+"\t"+reverseString(word)+"\t"+isPalindrome(word));
            System.out.println();
        }
    }
    public static String reverseString(String s){
        StringBuilder reverse = new StringBuilder();
        for (int i=s.length()-1;i>=0;i--){
            reverse.append(s.charAt(i));
        }
        return reverse.toString();
    }
    public static boolean isPalindrome(String s){
        if (s.length()%2==0) return s.substring(0,(s.length()/2)).equals(reverseString(s.substring((s.length()/2),s.length())));
        else return s.substring(0,(s.length()/2)).equals(reverseString(s.substring((s.length()/2)+1,s.length())));
    }
}
