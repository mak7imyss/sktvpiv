package lesson4;

/**
 * Напишите Java-программу для лексикографического сравнения двух строк, игнорируя различия в регистре.
 */
public class Task5 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "string";
        System.out.println("Result: " + str1.compareToIgnoreCase(str2));
    }
}
