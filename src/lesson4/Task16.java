package lesson4;

/**
 * Напишите программу на Java, которая возвращает true из заданной строки,
 * если первые два символа в строке также появляются в конце.
 */
public class Task16 {
    public static void main(String[] args) {
        String str = "StringSt";
        System.out.println("Result: " + str.substring(0,2).equals(str.substring(str.length()-2)));

    }
}
