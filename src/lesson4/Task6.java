package lesson4;

/**
 * Напишите программу на Java для объединения данной строки в конец другой строки.
 */
public class Task6 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "string";
        System.out.println("Result: " + str1 + str2);
    }
}
