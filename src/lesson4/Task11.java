package lesson4;

/**
 * Напишите программу на Java, чтобы преобразовать все символы строки в нижний регистр.
 */
public class Task11 {
    public static void main(String[] args) {
        String str = "String";
        System.out.println("Result: " + str.toLowerCase());
    }
}
