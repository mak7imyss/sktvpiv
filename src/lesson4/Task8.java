package lesson4;

/**
 * Напишите программу на Java, чтобы проверить, заканчивается ли данная строка содержимым другой строки.
 */
public class Task8 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "string";
        System.out.println("Result: " + str1.endsWith(str2));
    }
}
