package lesson4;

/**
 * Напишите программу на Java для печати после удаления дубликатов из заданной строки.
 */
public class Task14 {
    public static void main(String[] args) {
        String str = "SSttrriinngg";
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if(!result.contains(String.valueOf(str.charAt(i)))) {
                result += String.valueOf(str.charAt(i));
            }
        }
        System.out.println("Result: " + result);
    }
}
