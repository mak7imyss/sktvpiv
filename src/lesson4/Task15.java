package lesson4;

/**
 * Напишите программу на Java, чтобы найти первый неповторяющийся символ в строке.
 */
public class Task15 {
    public static void main(String[] args) {
        String str = "SSttrriinng";
        char out = 0;
        int length = str.length();
        for (int i = 0; i < length; i++) {
            String str1 = str.substring(0, i);
            String str2 = str.substring(i + 1);
            if (!(str1.contains(str.charAt(i) + "") || str2.contains(str
                    .charAt(i) + ""))) {
                out = str.charAt(i);
                break;
            }
        }
        System.out.println("Result: " + out);

    }
}
