package lesson4;

/**
 * Напишите программу на Java, которая возвращает сумму цифр, присутствующих в строке.
 * Если цифр нет, возвращаемая сумма равна 0.
 */
public class Task18 {
    public static void main(String[] args) {
        String str = "fg126fd61g6hfg6h";
        int sum = 0;
        for(int i = 0; i < str.length() ; i++){
            if( Character.isDigit(str.charAt(i)) ){
                sum = sum + Character.getNumericValue(str.charAt(i));
            }
        }
        System.out.println("Result: " + sum);
    }
}
