package lesson4;

import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Напишите программу на Java, чтобы найти второй по частоте символ в данной строке.
 */
public class Task13 {
    public static void main(String[] args) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        String s = "DfhhDDfgDsfhsasDg";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            Integer val = map.get(c);
            if (val != null) {
                map.put(c, new Integer(val + 1));
            } else {
                map.put(c, 1);
            }
        }
        int max = Collections.max(map.values());
        System.out.println("Result: " + map.entrySet().stream()
                .filter(entry -> entry.getValue() == max)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList()));
    }
}