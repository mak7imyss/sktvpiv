package lesson4;

/**
 * Напишите Java-программу для сравнения заданной строки с указанной последовательностью символов.
 */
public class Task7 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "string";
        System.out.println("Result: " + str1.contentEquals(str2));
    }
}
