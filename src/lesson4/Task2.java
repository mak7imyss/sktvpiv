package lesson4;

/**
 * Реализовать удаление повторяющихся символов в строке
 */
public class Task2 {
    public static void main(String[] args) {
        String str = "Stffrijjnvvg";
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.indexOf(str.charAt(i)) == str.lastIndexOf(str.charAt(i))) {
                result += str.charAt(i);
            }
        }
        System.out.println("\nOld string: " + str + "\nNew string: " + result);
    }
}
