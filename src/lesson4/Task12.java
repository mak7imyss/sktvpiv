package lesson4;

/**
 * Напишите программу на Java, чтобы преобразовать все символы в строке в верхний регистр.
 */
public class Task12 {
    public static void main(String[] args) {
        String str = "String";
        System.out.println("Result: " + str.toUpperCase());
    }
}
