package lesson4;

/**
 * Напишите программу на Java, чтобы проверить, содержат ли два объекта String одинаковые данные.
 */
public class Task9 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "string";
        System.out.println("Result: " + str1.equals(str2));
    }
}