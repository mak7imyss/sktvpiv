package lesson4;

import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Напишите программу на Java, которая возвращает количество символов, появляющихся три раза подряд в строке.
 */
public class Task17 {
    public static void main(String[] args) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        String s = "DfhhDDfgDsfhsasDg";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            Integer val = map.get(c);
            if (val != null) {
                map.put(c, new Integer(val + 1));
            } else {
                map.put(c, 1);
            }
        }
        System.out.println("Result: " + map.entrySet().stream()
                .filter(entry -> entry.getValue() == 3)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList()).size());

    }
}
