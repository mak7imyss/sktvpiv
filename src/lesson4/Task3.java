package lesson4;

import java.util.Arrays;

/**
 * Реализовать проверку, являются ли две строки анаграммами
 */
public class Task3 {
    public static void main(String[] args) {
        String str1 = "String";
        String str2 = "Sgntri";
        System.out.print("String1: " + str1 + "\nString2: " + str2 + "\nis anagramm: ");
        if (str1.length() == str2.length()) {
            char[] s1 = str1.toCharArray();
            char[] s2 = str2.toCharArray();
            Arrays.sort(s1);
            Arrays.sort(s2);
            if (Arrays.equals(s1, s2)) {
                System.out.print("true");
            } else {
                System.out.print("false");
            }
        } else {
            System.out.print("false");
        }
    }
}
