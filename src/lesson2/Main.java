package lesson2;

import java.util.ArrayList;
import java.util.List;
public class Main {
    // Создайте класс Phone, который содержит переменные number, model и weight.
    // Создайте три экземпляра этого класса.
    // Выведите на консоль значения их переменных.
    // Добавить в класс Phone методы: receiveCall, имеет один параметр – имя звонящего. Выводит на консоль сообщение “Звонит {name}”. getNumber – возвращает номер телефона. Вызвать эти методы для каждого из объектов.
    // Добавить конструктор в класс Phone, который принимает на вход три параметра для инициализации переменных класса - number, model и weight.
    // Добавить конструктор, который принимает на вход два параметра для инициализации переменных класса - number, model.
    // Добавить конструктор без параметров.
    // Вызвать из конструктора с тремя параметрами конструктор с двумя.
    // Добавьте перегруженный метод receiveCall, который принимает два параметра - имя звонящего и номер телефона звонящего. Вызвать этот метод.
    // Создать метод sendMessage с аргументами переменной длины. Данный метод принимает на вход номера телефонов, которым будет отправлено сообщение. Метод выводит на консоль номера этих телефонов.
    public static void main(String[] args) {
        Phone phone1 = new Phone("+79166191221", "A", 544);
        Phone phone2 = new Phone("+78005553535", "B");
        Phone phone3 = new Phone();

        phone1.showInfo();
        phone2.showInfo();
        phone3.showInfo();

        phone1.receiveCall("Oleg");
        phone1.receiveCall("Anton", "+77843658964");
        phone1.sendMessage("+75468547652", "+75486578145", "+77541658964");


        Veterinarian veterinarian = new Veterinarian();
        Animal cat = new Cat("Milk", "House", "Charm");
        Animal dog = new Dog("Bone", "Booth", "Breed");
        Animal horse = new Horse("Avena", "Stable ", "Speed");

        System.out.println();
        System.out.println();

        Animal[] animals = {cat, dog, horse};

        Veterinarian veterinar = new Veterinarian();
        for (Animal an : animals) veterinar.treatAnimal(an);

        System.out.println();

        List<Student> students = new ArrayList<>();
        students.add(new Student("Tom", "Ronnin", "G1", 4.3));
        students.add(new Student("Shon", "Verti", "G1", 5.0));
        students.add(new Aspirant("Rina", "larno", "G1", 5.0, "study1"));
        students.add(new Aspirant("Tony", "Tren", "G1", 4.9, "study2"));

        for (Student s : students) {
            s.showInfo();
        };
    }
}
