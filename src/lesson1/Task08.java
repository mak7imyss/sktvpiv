package lesson1;

public class Task08 {
    // Дан массив целых чисел: [1,-10,5,6,45,23, 45,-34,0,32,56,-1,2,-2]. Для данного массива найти и вывести на экран:
    // максимальное значение, сумму положительных элементов, сумму четных отрицательных элементов,
    // количество положительных элементов, среднее арифметическое отрицательных элементов
    public static void main(String[] args) {
        int[] arr = {1, -10, 5, 6, 45, 23, -45, -34, 0, 32, 56, -1, 2, -2};
        int maxVal = arr[0];
        int sumPosEl = 0;
        int sumEvenNegEl = 0;
        int numPosEl = 0;
            double meanNegEl = 0;
        int numEl = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > maxVal) {
                maxVal = arr[i];
            }
            if (arr[i] > 0) {
                sumPosEl = sumPosEl + arr[i];
            }
            if ((arr[i] < 0) & (arr[i] % 2 == 0)) {
                sumEvenNegEl = sumEvenNegEl + arr[i];
            }
            if (arr[i] > 0) {
                numPosEl++;
            }
            if (arr[i] < 0) {
                meanNegEl = meanNegEl + arr[i];
                numEl++;
            }
        }
        System.out.println("максимальное значение: " + maxVal);
        System.out.println("сумма положительных элементов: " + sumPosEl);
        System.out.println("сумму четных отрицательных элементов: " + sumEvenNegEl);
        System.out.println("количество положительных элементов: " + numPosEl);
        double number = (meanNegEl / numEl);
        System.out.println("среднее арифметическое отрицательных элементов: " + number);
    }
}
