package lesson1;

import java.util.Scanner;

public class Task04 {
    // Ввести целое число в консоли. Если оно является положительным, то прибавить к нему 1; если отрицательным, то вычесть из него 2;
    // если нулевым, то заменить его на 10. Вывести полученное число.
    public static void main(String[] args) {
        System.out.print("Введите целое число: ");
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        if (num > 0) {
            num = num + 1;
        }
        if (num < 0) {
            num = num - 2;
        }
        if (num == 0) {
            num = 10;
        }
        System.out.println(num);
    }
}
